var http = require('http');

var myServer = http.createServer((req,res)=>{
    res.writeHead(200,{"Content-Type":"text/html"})
    res.write('<h2>WELCOME TO NODEJS SERVER..</h2>');
    res.end();
})

myServer.listen(3000);
console.log('Server Started on port 3000');
