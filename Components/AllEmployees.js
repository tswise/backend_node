var express = require('express');
var dataFile = require('../data/data.json');

/*Get All Employees : localhost:3000/getAllEmployees */
let allemployees = express.Router().get("/", function(req, res) {
    res.send(dataFile.employees);
});
module.exports = allemployees;