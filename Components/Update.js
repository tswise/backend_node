var express = require('express');
var app = express();
var mongodb = require('mongodb');
let mongoClient = mongodb.MongoClient;

let update = express.Router().put("/", function(req, res) {
    mongoClient.connect("mongodb://localhost:27017/fsd56", (err, db) => {
        if (err)
            throw err;
        else {
            db.collection("employee").updateOne({"empId": req.body.empId},{$set: {
                "empName": req.body.empName, "salary": req.body.salary,
                "gender": req.body.gender, "city": req.body.city,
                "emailId": req.body.emailId, "password": req.body.password
            }},
                (err, data) => {
                    if (err)
                        throw err;
                    else {
                        res.send({ message: "1 document updated.." });
                    }
                });
        }
    });
});

module.exports = update;