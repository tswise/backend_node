var express = require('express');
var app = express();

var bodyparser = require("body-parser")
app.use(bodyparser.json())

let cors = require("cors");
app.use(cors());

app.use("/GetAllEmployees", require("./Components/AllEmployees"))
app.use("/getempbyid", require("./Components/GetEmployeeById"))
app.use("/fetch", require("./Components/Fetch"))
app.use("/register", require("./Components/Register"))
app.use("/del", require("./Components/Delete"))
app.use("/update", require("./Components/Update"))

app.server = app.listen(3001, function() {
    console.log("Server Started on Port Number: 3001");
    console.log("URL: http://localhost:3001");
});

